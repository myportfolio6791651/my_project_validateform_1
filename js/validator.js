"use strict";

document.addEventListener('DOMContentLoaded', () => {

    const translationTextForm = {
        'ua': {
            labelName: "Введіть Ваше ім'я:",
            labelPhone: "Введіть Ваш номер телефону:",
            examplePhone: "Приклад введення: +380ХХХХХХХХХ",
            labelEmail: "Введіть Вашу почту",
            submitBtn: "Відправити",
            patternChange: /^\+38\d{10}$/,
        },
        'en': {
            labelName: "Enter your name:",
            labelPhone: "Enter your phone number:",
            examplePhone: "Example: +1XXXXXXXXXX",
            labelEmail: "Enter your email:",
            submitBtn: "Submit",
            patternChange: /\+44\d{10,11}$/,
        },
        'hu': {
            labelName: "Adja meg a nevét:",
            labelPhone: "Adja meg a telefonszámát:",
            examplePhone: "Példa: +36XXXXXXXXX",
            labelEmail: "Adja meg az e-mail címét:",
            submitBtn: "Elküldés",
            patternChange: /^\+36\d{9}$/,
        },
    }

    function changeLang(phone) {
        const lang = document.documentElement.getAttribute(`lang`);
        let translation = translationTextForm[lang];

        if (!translation) {
            translation = translationTextForm['en'];
        }

        const labelNames = document.getElementById('names').innerHTML = translation.labelName;
        const labelPhones = document.getElementById('phones').innerHTML = translation.labelPhone;
        const examplePhones = document.getElementById('example').innerHTML = translation.examplePhone;
        const labelEmails = document.getElementById('emails').innerHTML = translation.labelEmail;
        const submitBtns = document.getElementById('submit').innerHTML = translation.submitBtn;

        let pattern = translation.patternChange;
        return pattern.test(phone);
    }

    changeLang()

    const form = document.getElementById('main_form');
    const name_validate = document.getElementById('name_person');
    const email_validate = document.getElementById('email_us');
    const phone_validate = document.getElementById('phone_number');
    const inputEmptys = document.querySelectorAll('.input_style');
    const loader_bg = document.querySelector(`body .loaderBg`);

    function emailValid(email) {
        let pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return pattern.test(email);
    }

    function emptyInputs(input) {
        return input.value == '';
    }

    function validateForm() {
        let error = 0;

        if (changeLang(phone_validate.value)) {
            phone_validate.style.boxShadow = '0rem 0rem .5rem green';
        } else {
            if (!emptyInputs(phone_validate) == '') {
                phone_validate.style.boxShadow = 'none';
                error++;
            } else {
                phone_validate.style.boxShadow = '0rem 0rem .5rem red';
                error++;
            }
        }

        if (emailValid(email_validate.value)) {
            email_validate.style.boxShadow = '0rem 0rem .5rem green';
        } else {
            if (!emptyInputs(email_validate) == '') {
                email_validate.style.boxShadow = 'none';
                error++;
            } else {
                email_validate.style.boxShadow = '0rem 0rem .5rem red';
                error++;
            }
        }

        if (name_validate.value !== '') {
            name_validate.style.boxShadow = '0rem 0rem .5rem green';
        } else {
            name_validate.style.boxShadow = 'none';
            error++;
        }

        return error;
    }

    phone_validate.addEventListener('input', validateForm);
    email_validate.addEventListener('input', validateForm);
    name_validate.addEventListener('input', validateForm);

    form.addEventListener(`submit`, formSubmit);

    async function formSubmit(e) {
        e.preventDefault();

        let error = validateForm();

        let formData = new FormData(form);

        if (error === 0) {
            loader_bg.style.display = `block`;
            let response = await fetch('./php/index.php', {
                method: 'POST',
                body: formData
            });
            if (response.ok) {
                let result = await response.json();
                alert(response.message);
                form.reset();
                loader_bg.style.display = `none`;
            } else {
                loader_bg.style.display = `none`;
                alert(`Ошибка отправки`);
            }
        } else {
            inputEmptys.forEach(el => {
                if (el.value == '') {
                    el.style.boxShadow = '0rem 0rem .5rem red';
                }
            })
        }
    }
});